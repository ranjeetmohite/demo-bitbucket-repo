import React, { useState,  useReducer } from 'react';
import ReactDOM from "react-dom";
import "./Table.css";
import NavBar from "../NavBar/NavBar";
import UpdateForm from "../Form/UpdateForm";

//using Reducer function
// const reducer = (state, action) => {
//     let keys = Object.keys(localStorage);
//     let i = keys.length;
//     if (action === "setList") {
//         state = [];
//         while (i--) {

//             if (i === 0) {
//                 continue;
//             }

//             state.push(JSON.parse(localStorage.getItem(keys[i])));
//         }
//         return state;
//     }
//     return state;
// }

export default function Table() {

    // for (let i = 1; i <= localStorage.length; i++) {
    //     const obj = JSON.parse(localStorage.key(i));
    //     array[i] = obj;
    // }


    const getArrayData = () => {
        let keys = Object.keys(localStorage);
        // console.log(keys);
        let i = keys.length;

        let array = [];
        while (i--) {

            if (i === 0) {
                continue;
            }

            array.push(JSON.parse(localStorage.getItem(keys[i])));
        }

        // Sorting the array
        /**
         * This sort function checks id of first(a) and second(b) elements and sort them in ascending order
         */
        array.sort((a, b) => {
            return a.id - b.id;
        });

        return array;
    }

    //creating state for array
    const [list, setList] = useState(getArrayData());

    //using useReducer hook
    // const [list, setList] = useReducer(reducer, getArrayData());

    // console.log(list);


    //Editing Records
    const editHandler = (id) => {
        return(
            ReactDOM.render(
                <>
                <NavBar />
                <UpdateForm id = {id}/>
                </>,
                document.getElementById("root")
            )
        );

    }


    //removing the element from localstorage
    const removeHandler = (id) => {

        localStorage.removeItem(id);
        // setList("setList");
        setList(getArrayData);
        window.alert("Record Removed from Local Storage");
    }


    return (
        <div className="MainContainer my-4 mx-4">

            <table className="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Emp ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Contact</th>
                        <th scope="col">Email</th>
                        <th scope="col">Address</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Date of Birth</th>
                        <th scope="col" colSpan="2" style={{ textAlign: "center" }}>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        localStorage.length > 1 &&
                        list.map(
                            element => {
                                return (
                                    <tr>
                                        <th scope="row">{element.id}</th>
                                        <td>{element.name}</td>
                                        <td>{element.contact}</td>
                                        <td>{element.email}</td>
                                        <td>{element.address}</td>
                                        <td>{element.gender}</td>
                                        <td>{element.dob}</td>
                                        <td style={{ width: "15px" }}>
                                            <button style={{ width: "80px" }} type="button" class="btn btn-primary btnPrimary" onClick={() => editHandler(element.id)}>Edit</button>
                                        </td>
                                        <td style={{ width: "15px" }}>
                                            <button style={{ width: "80px" }} type="button" class="btn btn-danger" onClick={() => removeHandler(element.id)}>Remove</button>
                                        </td>

                                    </tr>
                                )//end return of arrow function
                            }//end arrow function
                        )//end map function
                    }


                </tbody>
            </table>
        </div>
    )
}
