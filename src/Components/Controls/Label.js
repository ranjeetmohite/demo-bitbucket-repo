import React from 'react'

export default function Label(props) {
    return (
        <label for={props.for} class="form-label">{props.children}</label>
    )
}
