import React, {useRef} from 'react'
import "./Input.css";

export default function Input(props) {
    const refVal = useRef(props.ref);
    return (
        <input type= {props.type} ref = {refVal} onChange={()=>refVal.current.value} class="form-control input" id={props.id} placeholder={props.placeholder} required/>
    );
}
