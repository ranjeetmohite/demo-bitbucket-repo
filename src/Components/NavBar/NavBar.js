import React from 'react';
import "./NavBar.css";
import ReactDOM from "react-dom";
import Table from '../Table/Table';
import Form from '../Form/Form';

export default function NavBar() {

    const clickHandler = (action) =>
    {
        if(action === "showEmp")
        {
            return(
                ReactDOM.render(
                   <> <NavBar/>
                    <Table /></>,
                    document.getElementById("root")
                )
            );
        }
        else if(action === "addEmp")
        {
            return(
                ReactDOM.render(
                   <> <NavBar/>
                    <Form /></>,
                    document.getElementById("root")
                )
            );
        }
    }

    return (
        <>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark ">
            <div className="container-fluid">
                <label className="navbar-brand">Fathom Software</label>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <label className="nav-link active hoverLabel" aria-current="page" onClick={() => clickHandler("addEmp")}>Add Employee</label>
                        </li>
                        <li className="nav-item">
                            <label className="nav-link active hoverLabel" onClick={() => clickHandler("showEmp")}>All Employees</label>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        
        </>
    );
}
