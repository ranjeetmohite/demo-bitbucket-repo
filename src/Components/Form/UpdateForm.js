import React, { useRef, useState } from 'react'
// import Input from '../Controls/Input';
import Label from '../Controls/Label';
import "./Form.css";
import ReactDOM from "react-dom";
import NavBar from "../NavBar/NavBar";
import Table from '../Table/Table';

export default function UpdateForm(props) {

    //getting data related with id
    const element = JSON.parse(localStorage.getItem(props.id));

    //setting references for all input tags
    const id = useRef();
    const [name1,setName] = useState(element.name);
    const [contact1,setContact] = useState(element.contact);
    const [address1,setAddress] = useState(element.address);
    const [email1,setEmail] = useState(element.email);
    const [gender1,setGender] = useState(element.gender);
    const [dob1,setDob] = useState(element.dob);
    const [role1,setRole] = useState(element.role);
    const [salary1,setSalary] = useState(element.salary);
    const [desc1,setDesc] = useState(element.desc);



    //creating on change handlers
    const nameHandler = event =>{
        setName(event.target.value);
    }

    const contactHandler = event =>{
        setContact(event.target.value);
    }

    const addressHandler = event =>{
        setAddress(event.target.value);
    }

    const emailHandler = event =>{
        setEmail(event.target.value);
    }

    const genderHandler = event =>{
        setGender(event.target.value);
    }

    const dobHandler = event =>{
        setDob(event.target.value);
    }

    const roleHandler = event =>{
        setRole(event.target.value);
    }

    const salaryHandler = event =>{
        setSalary(event.target.value);
    }

    const descHandler = event =>{
        setDesc(event.target.value);
    }

    //handling back event
    const backHandler = () =>{

        return(
            ReactDOM.render(
                <>
                    <NavBar/>
                    <Table/>
                </>,
                document.getElementById("root")
            )
        );

    }//end backHandler

    const submitHandler = (event) => {
        event.preventDefault();




        //getting all from data
        const obj = {
            id: id.current.value,
            name: name1,
            contact: contact1,
            address: address1,
            email: email1,
            gender: gender1,
            dob: dob1,
            role: role1,
            salary: salary1,
            desc: desc1
        };

        //converting js object to JSON string
        const jsonStr = JSON.stringify(obj);

        //storing JSON string to local storage
        localStorage.setItem(id.current.value, jsonStr);

        window.alert("Record Updated Successfully...");

        return(
            ReactDOM.render(
                <>
                    <NavBar/>
                    <Table/>
                </>,
                document.getElementById("root")
            )
        );


    }//end submit handler

    

    return (

        <form onSubmit={submitHandler} className="register-form" id="register-form">

            <h1 className="title">Update Employee Information</h1>
            <hr />

            <table className="table">
                <tbody>
                    <tr>
                        <td colSpan="2">
                            <h2>Personal Information</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Label for="id">Employee ID</Label>

                            <input type="number" ref={id} className="form-control input" id="id" value={props.id} readOnly />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Label for="name">Employee Name</Label>

                            <input type="text" value={name1} onChange={nameHandler} className="form-control input" id="name" placeholder="Enter Name" required/>
                        </td>

                        <td>
                            <Label for="contact">Contact</Label>

                            <input type="number" value={contact1} onChange={contactHandler} className="form-control input" id="contact" placeholder="Enter Contact Number" required/>
                        </td>
                    </tr>
                    <tr>
                        <td colSpan="2">
                            <Label for="address">Address</Label>

                            <textarea class="form-control" value={address1} onChange={addressHandler} className="form-control input" placeholder="Enter Address" id="address" required></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Label for="email">Email</Label>

                            <input type="email" id="email"  value={email1} onChange={emailHandler} className="form-control input" placeholder="Enter Email" required/>
                        </td>
                        <td>
                            <Label for="gender">Gender</Label>

                            <div class="form-check">
                                <input class="form-check-input" type="radio" checked = {gender1 === "Male"} onChange={genderHandler} name="gender" id="male" value="Male" />
                                <label class="form-check-label" for="gender">
                                    Male
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" checked = {gender1 === "Female"} onChange={genderHandler} name="gender" id="female" value="Female" />
                                <label class="form-check-label" for="gender">
                                    Female
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colSpan="2">
                            <Label for="dob">Birth Date</Label>

                            <input type="date"  value={dob1} onChange={dobHandler} className="form-control input" id="date" placeholder="DOB" required/>
                        </td>
                    </tr>
                    <tr>
                        <td colSpan="2">
                            <h2>Job Profile</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Label for="role">Job Role</Label>

                            <input type="text" id="role" value={role1} onChange={roleHandler} className="form-control input" placeholder="Enter Job Role" required/>
                        </td>

                        <td>
                            <Label for="salary">Salary</Label>

                            <input type="number" id="salary" value={salary1} onChange={salaryHandler} className="form-control input" placeholder="Enter Salary" required/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <Label for="job">Job Description</Label>

                            <textarea class="form-control"  value={desc1} onChange={descHandler} placeholder="Your Job Description Here..." id="job" required></textarea>
                        </td>
                        <td>
                            <button type="submit" class="btn btn-primary">Update Data</button><br />
                            <button type="button" onClick={backHandler} class="btn btn-secondary">Back</button>
                        </td>
                    </tr>

                </tbody>
            </table>



        </form>

    )
}
