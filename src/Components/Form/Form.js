import React, { useRef, useState } from 'react'
// import ReactDOM from 'react-dom';
// import Input from '../Controls/Input';
import Label from '../Controls/Label';
// import Alert from './Alert';
import "./Form.css";

export default function Form() {

    //setting references for all input tags
    const name = useRef();
    const contact = useRef();
    const address = useRef();
    const email = useRef();
    const gender = useRef();
    const dob = useRef();
    const role = useRef();
    const salary = useRef();
    const desc = useRef();

    //creating state for alert
    // const [alert, setAlert] = useState(null);
    // const showAlert = () => {
    //     setAlert(
    //         {
    //             alertType: "success",
    //             type: "Success! ",
    //             message: "Record Inserted Successfully!"
    //         }
    //     );
    //     setTimeout(() => {

    //         setAlert(null);

    //     }, 2500)//end timeout

    // }  //end show alert  

    const submitHandler = (event) => {
        event.preventDefault();

        //getting max id/key from local storage to set employee id
        // let id = localStorage.length + 1;
        // let id = parseInt(Math.random());
        let id = getID();

        // console.log(
        //     "Name: "+name.current.value+"\nContact: "+contact.current.value
        // );

        //getting all from data
        const obj = {
            id: id,
            name: name.current.value,
            contact: contact.current.value,
            address: address.current.value,
            email: email.current.value,
            gender: gender.current.value,
            dob: dob.current.value,
            role: role.current.value,
            salary: salary.current.value,
            desc: desc.current.value
        };

        //converting js object to JSON string
        const jsonStr = JSON.stringify(obj);

        //storing JSON string to local storage
        localStorage.setItem(id, jsonStr);

        //chinging id in local storage to get next id at 0th key
        localStorage.setItem(0, id);


        //clearing all input fields
        name.current.value = "";
        contact.current.value = "";
        address.current.value = "";
        email.current.value = "";
        dob.current.value = "";
        role.current.value = "";
        salary.current.value = "";
        desc.current.value = "";

        // // clearing radio button selection
        document.getElementById("male").checked = false;
        document.getElementById("female").checked = false;

        window.alert("Success! Record Inserted Successfully...");
        // showAlert();//showing alert

    }

    return (
        <>

            <form onSubmit={submitHandler} className="register-form" id="register-form">

                <h1 className="title">Employee Registration</h1>
                <hr />

                <table className="table">
                    <tbody>
                        <tr>
                            <td colSpan="2">
                                <h2>Personal Information</h2>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Label for="name">Employee Name</Label>

                                <input type="text" ref={name} className="form-control input" id="name" placeholder="Enter Name" required />
                            </td>

                            <td>
                                <Label for="contact">Contact</Label>

                                <input type="number" ref={contact} className="form-control input" id="contact" placeholder="Enter Contact Number" required />
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="2">
                                <Label for="address">Address</Label>

                                <textarea class="form-control" ref={address} className="form-control input" placeholder="Enter Address" id="address" required></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Label for="email">Email</Label>

                                <input type="email" id="email" ref={email} className="form-control input" placeholder="Enter Email" required />
                            </td>
                            <td>
                                <Label for="gender">Gender</Label>

                                <div class="form-check">
                                    <input class="form-check-input" type="radio" ref={gender} name="gender" id="male" value="Male" />
                                    <label class="form-check-label" for="gender">
                                        Male
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" ref={gender} name="gender" id="female" value="Female" />
                                    <label class="form-check-label" for="gender">
                                        Female
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="2">
                                <Label for="dob">Birth Date</Label>

                                <input type="date" ref={dob} className="form-control input" id="date" placeholder="DOB" required />
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="2">
                                <h2>Job Profile</h2>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Label for="role">Job Role</Label>

                                <input type="text" id="role" ref={role} className="form-control input" placeholder="Enter Job Role" required />
                            </td>

                            <td>
                                <Label for="salary">Salary</Label>

                                <input type="number" id="salary" ref={salary} className="form-control input" placeholder="Enter Salary" required />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Label for="job">Job Description</Label>

                                <textarea class="form-control" ref={desc} placeholder="Your Job Description Here..." id="job" required></textarea>
                            </td>
                            <td>
                                <button type="submit" class="btn btn-primary">Register Now</button><br />
                                <button type="reset" class="btn btn-secondary">Reset</button>
                            </td>
                        </tr>

                    </tbody>
                </table>



            </form>
            {/* Calling alert
            <Alert alert={alert} /> */}

        </>
    )
}

const getID = () => {

    //if element is not present in 0th key then return id=1
    if (!localStorage.getItem(0)) {
        return 1;
    }

    //else return id
    return parseInt(localStorage.getItem(0)) + 1;
}