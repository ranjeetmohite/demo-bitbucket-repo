import Form from "./Components/Form/Form";
import NavBar from "./Components/NavBar/NavBar";

function App() {

 
  return (
    <div>
      <NavBar/>
      <div id="portalDiv"></div>
      <Form/>
    </div>
  );
}

export default App;
